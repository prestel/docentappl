\documentclass[11pt]{article}
%\pagestyle{empty}

\usepackage[margin=1.1in]{geometry}
\usepackage{todonotes}
\let\OLDthebibliography\thebibliography
\renewcommand\thebibliography[1]{
  \OLDthebibliography{#1}
  \setlength{\parskip}{0pt}
  \setlength{\itemsep}{0pt plus 0.3ex}
}

\usepackage{graphicx}
\usepackage{tikz}
\usepackage{overpic}
\usepackage{relsize}
\usepackage{xspace}
\usepackage{cite}
%\usepackage{subfigure}
\usepackage{subcaption}
\usepackage{enumitem}
\usepackage[export]{adjustbox}
\usepackage{fancyvrb}
 \usepackage[pdfborder={0 0 0}, colorlinks=true,
         urlcolor=blue]{hyperref}
\hypersetup{
  pdfauthor={Stefan Prestel},
  pdftitle={VR starting grant: Publications},
}

\definecolor{mygrey}{rgb}{0.85,0.85,0.8}
\definecolor{mydarkgrey}{rgb}{0.4,0.4,0.35}

\tikzstyle{all_style} = [mydarkgrey, line width = 0.8mm]
\tikzstyle{arrow_style} = [all_style, ->, >=latex]

\usepackage{etoolbox}
%\newcommand{\circled}[2][]{%
%  \tikz[baseline=(char.base)]{%
%    \node[shape = circle, draw, inner sep = 1pt]
%    (char) {\phantom{\ifblank{#1}{#2}{#1}}};%
%    \node at (char.center) {\makebox[0pt][c]{#2}};}}
%\robustify{\circled}
\newcommand{\circled}[2][]{%
  \tikz[baseline=(char.base)]{%
    \node[shape = circle, draw, inner sep = 1pt]
    (char) {\phantom{\ifblank{\smaller \smaller #1}{\smaller \smaller #2}{\smaller \smaller #1}}};%
    \node at (char.center) {\makebox[0pt][c]{\smaller \smaller #2}};}}
\robustify{\circled}

\newcommand{\dcircled}[1]{\circled[10]{#1}}

\newcommand{\Herwig}{H{\smaller ERWIG}\xspace}
\newcommand{\Pythia}{P{\smaller YTHIA}\xspace}
\newcommand{\Sherpa}{S{\smaller HERPA}\xspace}
\newcommand{\pythia}{\Pythia}
\newcommand{\sherpa}{\Sherpa}

\newcommand{\Vincia}{V{\smaller INCIA}\xspace}
\newcommand{\Dire}{D{\smaller IRE}\xspace}
\newcommand{\vincia}{\Vincia}
\newcommand{\dire}{\Dire}

\newcommand{\MG}{M{\smaller ADGRAPH}5\xspace}
\newcommand{\mg}{\MG}

%\begin{document}
%\mbox{}\rlap{\rule{.7\linewidth}{.4pt}}%
%The original version: \circled{1} and \circled{10}.
%\mbox{}\rlap{\rule{.7\linewidth}{.4pt}}%
%The improved version: \circled[10]{1} and \circled[10]{10}.
%\newcommand{\dcircled}[1]{\circled[00]{#1}}

\def\dateitem#1#2{\parbox[t]{2.8cm}{#1}   &
  \parbox[t]{\textwidth-4cm}{#2}\\}
\def\eduitem#1#2#3{\parbox[t]{2.8cm}{#1}   & {{\bf #2}\
    {#3\hfill\hfill}}\\}
\def\scienceitem#1#2{{\bf #1}\quad
  \parbox[t]{\textwidth-\widthof{\bf #1}-12mm}{#2}\\}

\newcommand*{\inspireurl}[1]{\\\href{#1}{INSPIRE-HEP entry}}
\newcommand*{\citations}[1]{\\* #1}

\begin{document}

\noindent
Please find below the list of selected and relevant publications. Only
publications that were published at the date of writing (April 31, 2020) 
have been included.

\section{Selected publications}

Please find below selected publications of relevance to the proposal.

\renewcommand{\labelenumi}{[\arabic{enumi}]~~}
\begin{enumerate}
\item {\bf S.~Prestel} and M.~Spannowsky.
``HYTREES: Combining Matrix Elements and Parton Shower for Hypothesis Testing''.
  Eur. Phys. J. C 79 (2019) no.7, 546,
  DOI: 10.1140/epjc/s10052-019-7030-y\\
  This article introduces a new way method to perform hypothesis tests on 
  collider data, using state-of-the-art developments in  
  perturbation theory (see e.g. [3] below) to construct a \underline{define a theoretically well-defined calculable 
  classifier}. This is the first time a matrix-element classifier that 
  exhibits fixed-order accuracy, detailed all-order corrections and quantum
  interference has been obtained.
  Extending this proof-of-principle work and finding new 
  theoretically well-defined generic classification methods
  for Standard-Model (SM) and Beyond-the-SM signals is a key objective of the
  current proposal.
\item F.~Dulat, S.~H\"{o}che and {\bf S.~Prestel}, 
``Leading-color fully differential two-loop soft corrections to QCD dipole showers'',
  Phys. Rev. D 98 (2018) 7, 074013,
  DOI: 10.1103/PhysRevD.98.074013\\
  This article develops a new method to construct numerical,
  completely differential, higher-order resummation tools. This is the first
  time that a parton shower algorithm can achieve NLO precision and thus 
  contains a controlled uncertainty estimate. As such, it is important for
  future LHC searches, and represents
  a significant step forward for event generation techniques. The structure
  of the simultaneous double-emission corrections is identical to complicated
  SMEFT operators, and is thus a \underline{crucial ingredient for SMEFT evolution}.
\item N.~Fischer, {\bf S.~Prestel},
  ``Combining states without scale hierarchies with ordered parton showers'',
  Eur. Phys. J. C 77 (2017) no.9, 601,
  DOI: 10.1140/epjc/s10052-017-5160-7\\
  This articles highlights that parton showers can be supplemented by exact
  quantum-mechanical \underline{interference effects with matrix element corrections}, 
  without introducing any auxiliary parameters. 
  As such, this is a crucial step towards including exact
  quantum-mechanical interference also for likelihood-based hypothesis 
  testing tools based on exact transition matrix elements.
  It further highlights my involvement in the \vincia 
  parton shower plugin for \pythia. 
\item J.~R.~Christiansen and {\bf S.~Prestel},
  ``Merging weak and QCD showers with matrix elements",
  Eur. Phys. J. C76 (2016) no.1, 39,
  DOI: 10.1140/epjc/s10052-015-3871-1\\
  This publication embarks on a consistent combination of QCD and electroweak
  evolution for the specific processes of multiple weak bosons in conjunction
  with multiple well-separated jets. As such, it is to date the only complete
  calculation that includes multi-jet matrix elements and electroweak all-order
  effects, without approximating the phase-space dependence of the electroweak
  evolution. The study highlights the need for a \underline{proper treatment of electroweak
  effects} at the LHC, and is thus a stepping stone for the current proposal.
%\item R. Frederix, S. Frixione, A. S. Papanastasiou, {\bf S. Prestel}, P. Torrielli,
%  ``Off-shell single-top production at NLO matched to parton showers",
%  JHEP 1606 (2016) 027,\\
%  DOI: 10.1007/JHEP06(2016)027 \\
%  This article exposes and addresses inconsistencies in the modeling of 
%  intermediate resonances in precision event generator simulations. In 
%  particular, choices in the modeling of off-shell electroweak top quark decays
%  are highlighted, and the ensuing uncertainties quantified. One conclusion of this
%  study is that precision top quark predictions (and thus indirect measurements)
%  require a consistent understanding of off-shell electroweak decays.
%  Furthermore, the publication highlights my long-term involvement in the 
%  \textsc{Madgraph5}-{\smaller{a}}\textsc{Mc@nlo} project.

%\cite{Hoche:2015sya}
\item%{Hoche:2015sya}
S.~H{\"o}che, {\bf S.~Prestel},
  ``The midpoint between dipole and parton showers'',
  Eur. Phys. J. C75 (2015) no.9, 461
  DOI: 10.1140/epjc/s10052-015-3684-2\\
  This article is relevant for my development, since it showcases the first 
  open-source software project developed by the applicant
  alone. This software is technical framework for the the articles [1], [2] and [3]
  in this list, and is designed to be maximally flexible, such that \underline{handling
  the evolution of the SM and large classes of BSM models} becomes feasible.
\item T.~Sj\"{o}strand, S.~Ask, J.~R.~Christiansen, R.~Corke, N.~Desai, P.~Ilten, S.~Mrenna,
  {\bf S.~Prestel}, C.~O.~Rasmussen, P.~Z.~Skands,
  ``An Introduction to PYTHIA 8.2'',
  %arXiv:1410.3012 [hep-ph],
  Comput. Phys. Commun. 191 (2015) 159,
  DOI: 10.1016/j.cpc.2015.01.024\\
  This article describes the phenomena modeled by, and calculations necessary in,
  the \pythia~8.2 General-Purpose Event Generator simulation tool. \pythia~8.2 
  has since seen rapid adoption by the high-energy physics community, because
  of the versatility of the tool and because of continued development and
  code support.
  This article was included to highlight my \underline{commitment to the
  \pythia event generator}, since it is formally the first \pythia
  manual after I have started contributing heavily to the framework. 
\item S.~H\"{o}che, Y.~Li, {\bf S.~Prestel},
  ``Drell-Yan lepton pair production at NNLO QCD with parton showers'',
  %arXiv:1405.3607 [hep-ph],
  Phys. Rev. D 91 (2015) 7, 074015,
  DOI: 10.1103/PhysRevD.91.074015\\
  This article introduces a cross-section calculation at highest 
  precision into a completely differential event generator. This is
  the first calculation that consistently combines a {\smaller NNLO}
  calculation with a multiparton interaction model and hadronization. Furthermore,
  the article highlights my \underline{involvement in the Sherpa event generator}.
\item L.~L\"{o}nnblad, {\bf S.~Prestel},
  ``Merging Multi-leg NLO Matrix Elements with Parton Showers'',
  %arXiv:1211.7278 [hep-ph],
  JHEP 1303 (2013) 166,
  DOI: 10.1007/JHEP03(2013)166, 
  {\bf (Part of PhD thesis)}\\
  This article represented an important jump in the accuracy of event
  generation frameworks, and resulted in the first publicly available 
  next-to-leading order multi-jet merging code for LHC applications. 
  The resulting method still represents the state-of-the art of \underline{high-precision
  event generation}, and has inspired further methods in the MC community, e.g.~item
  [7] of this selection. 
\end{enumerate}

%\vspace*{10ex}
%\newpage
\section{Relevant publications of the last eight years}

Please find below relevant publications of the last eight years. I hope
these publications convey my expertise in designing high-energy physics event
generators and in precision SM calculations, but also convey broader relevance
and interests.


\subsection{Peer-reviewed original articles}

\begin{enumerate}



%\cite{Frederix:2020trv}
\item%{Frederix:2020trv}
R.~Frederix, S.~Frixione, S.~Prestel and P.~Torrielli,
\emph{``On the reduction of negative weights in MC@NLO-type matching procedures''},
[arXiv:2002.12716 [hep-ph]].
\citations{1 citations counted in INSPIRE as of 30 Mar 2020}

%\cite{Gellersen:2020tdj}
\item%{Gellersen:2020tdj}
L.~Gellersen and S.~Prestel,
\emph{``Scale and Scheme Variations in Unitarized NLO Merging''},
[arXiv:2001.10746 [hep-ph]].
\citations{No citations counted in INSPIRE as of 25 Mar 2020}

%\cite{Hoeche:2019rti}
\item%{Hoeche:2019rti}
S.~H{\"o}che, S.~Prestel and H.~Schulz,
\emph{``Simulation of Vector Boson Plus Many Jet Final States at the High Luminosity LHC''},
Phys.\ Rev.\ D \textbf{100} (2019) no.1, 014024
doi:10.1103/PhysRevD.100.014024
[arXiv:1905.05120 [hep-ph]].
\citations{6 citations counted in INSPIRE as of 25 Mar 2020}





%\cite{Prestel:2019neg}
\item%{Prestel:2019neg}
S.~Prestel and M.~Spannowsky,
\emph{``HYTREES: Combining Matrix Elements and Parton Shower for Hypothesis Testing''},
Eur.\ Phys.\ J.\ C \textbf{79} (2019) no.7, 546
doi:10.1140/epjc/s10052-019-7030-y
[arXiv:1901.11035 [hep-ph]].
\citations{5 citations counted in INSPIRE as of 25 Mar 2020}

%\cite{Isaacson:2018zdi}
\item%{Isaacson:2018zdi}
J.~Isaacson and S.~Prestel,
\emph{``Stochastically sampling color configurations''},
Phys.\ Rev.\ D \textbf{99} (2019) no.1, 014021
doi:10.1103/PhysRevD.99.014021
[arXiv:1806.10102 [hep-ph]].
\citations{14 citations counted in INSPIRE as of 25 Mar 2020}

%\cite{Dulat:2018vuy}
\item%{Dulat:2018vuy}
F.~Dulat, S.~H{\"o}che and S.~Prestel,
\emph{``Leading-Color Fully Differential Two-Loop Soft Corrections to QCD Dipole Showers''},
Phys.\ Rev.\ D \textbf{98} (2018) no.7, 074013
doi:10.1103/PhysRevD.98.074013
[arXiv:1805.03757 [hep-ph]].
\citations{17 citations counted in INSPIRE as of 25 Mar 2020}



%\cite{Fischer:2017yja}
\item%{Fischer:2017yja}
N.~Fischer and S.~Prestel,
\emph{``Combining states without scale hierarchies with ordered parton showers''},
Eur.\ Phys.\ J.\ C \textbf{77} (2017) no.9, 601
doi:10.1140/epjc/s10052-017-5160-7
[arXiv:1706.06218 [hep-ph]].
\citations{7 citations counted in INSPIRE as of 25 Mar 2020}

%\cite{Hoche:2017hno}
\item%{Hoche:2017hno}
S.~H{\"o}che, F.~Krauss and S.~Prestel,
\emph{``Implementing NLO DGLAP evolution in Parton Showers''},
JHEP \textbf{10} (2017), 093
doi:10.1007/JHEP10(2017)093
[arXiv:1705.00982 [hep-ph]].
\citations{37 citations counted in INSPIRE as of 25 Mar 2020}

%\cite{Hoche:2017iem}
\item%{Hoche:2017iem}
S.~H{\"o}che and S.~Prestel,
\emph{``Triple collinear emissions in parton showers''},
Phys.\ Rev.\ D \textbf{96} (2017) no.7, 074017
doi:10.1103/PhysRevD.96.074017
[arXiv:1705.00742 [hep-ph]].
\citations{37 citations counted in INSPIRE as of 25 Mar 2020}

%\cite{Alioli:2016fum}
\item%{Alioli:2016fum}
S.~Alioli {\it et al.},
\emph{``Precision studies of observables in $p p \rightarrow W \rightarrow l\nu _l$ and $ pp \rightarrow \gamma ,Z \rightarrow l^+ l^-$ processes at the LHC''},
Eur.\ Phys.\ J.\ C \textbf{77} (2017) no.5, 280
doi:10.1140/epjc/s10052-017-4832-7
[arXiv:1606.02330 [hep-ph]].
\citations{63 citations counted in INSPIRE as of 25 Mar 2020}

%\cite{Fischer:2016vfv}
\item%{Fischer:2016vfv}
N.~Fischer, S.~Prestel, M.~Ritzmann and P.~Skands,
\emph{``Vincia for Hadron Colliders''},
Eur.\ Phys.\ J.\ C \textbf{76} (2016) no.11, 589
doi:10.1140/epjc/s10052-016-4429-6
[arXiv:1605.06142 [hep-ph]].
\citations{40 citations counted in INSPIRE as of 25 Mar 2020}



%\cite{Frederix:2016rdc}
\item%{Frederix:2016rdc}
R.~Frederix, S.~Frixione, A.~S.~Papanastasiou, S.~Prestel and P.~Torrielli,
\emph{``Off-shell single-top production at NLO matched to parton showers''},
JHEP \textbf{06} (2016), 027
doi:10.1007/JHEP06(2016)027
[arXiv:1603.01178 [hep-ph]].
\citations{49 citations counted in INSPIRE as of 30 Mar 2020}

%\cite{Frederix:2015eii}
\item%{Frederix:2015eii}
R.~Frederix, S.~Frixione, A.~Papaefstathiou, S.~Prestel and P.~Torrielli,
\emph{``A study of multi-jet production in association with an electroweak vector boson''},
JHEP \textbf{02} (2016), 131
doi:10.1007/JHEP02(2016)131
[arXiv:1511.00847 [hep-ph]].
\citations{19 citations counted in INSPIRE as of 25 Mar 2020}

%\cite{Christiansen:2015jpa}
\item%{Christiansen:2015jpa}
J.~R.~Christiansen and S.~Prestel,
\emph{``Merging weak and QCD showers with matrix elements''},
Eur.\ Phys.\ J.\ C \textbf{76} (2016) no.1, 39
doi:10.1140/epjc/s10052-015-3871-1
[arXiv:1510.01517 [hep-ph]].
\citations{10 citations counted in INSPIRE as of 25 Mar 2020}

%\cite{Hoche:2015sya}
\item%{Hoche:2015sya}
S.~H{\"o}che and S.~Prestel,
\emph{``The midpoint between dipole and parton showers''},
Eur.\ Phys.\ J.\ C \textbf{75} (2015) no.9, 461
doi:10.1140/epjc/s10052-015-3684-2
[arXiv:1506.05057 [hep-ph]].
\citations{78 citations counted in INSPIRE as of 25 Mar 2020}

%\cite{Sjostrand:2014zea}
\item%{Sjostrand:2014zea}
T.~Sj{\"o}strand, S.~Ask, J.~R.~Christiansen, R.~Corke, N.~Desai, P.~Ilten, S.~Mrenna, S.~Prestel, C.~O.~Rasmussen and P.~Z.~Skands,
\emph{``An Introduction to PYTHIA 8.2''},
Comput.\ Phys.\ Commun.\  \textbf{191} (2015), 159-177
doi:10.1016/j.cpc.2015.01.024
[arXiv:1410.3012 [hep-ph]].
\citations{2265 citations counted in INSPIRE as of 31 Mar 2020}

%\cite{Hoche:2014dla}
\item%{Hoche:2014dla}
S.~H{\"o}che, Y.~Li and S.~Prestel,
\emph{``Higgs-boson production through gluon fusion at NNLO QCD with parton showers''},
Phys.\ Rev.\ D \textbf{90} (2014) no.5, 054011
doi:10.1103/PhysRevD.90.054011
[arXiv:1407.3773 [hep-ph]].
\citations{63 citations counted in INSPIRE as of 25 Mar 2020}

%\cite{Hoeche:2014aia}
\item%{Hoeche:2014aia}
S.~H{\"o}che, Y.~Li and S.~Prestel,
\emph{``Drell-Yan lepton pair production at NNLO QCD with parton showers''},
Phys.\ Rev.\ D \textbf{91} (2015) no.7, 074015
doi:10.1103/PhysRevD.91.074015
[arXiv:1405.3607 [hep-ph]].
\citations{87 citations counted in INSPIRE as of 25 Mar 2020}



%\cite{Karneyeu:2013aha}
\item%{Karneyeu:2013aha}
A.~Karneyeu, L.~Mijovic, S.~Prestel and P.~Skands,
\emph{``MCPLOTS: a particle physics resource based on volunteer computing''},
Eur.\ Phys.\ J.\ C \textbf{74} (2014), 2714
doi:10.1140/epjc/s10052-014-2714-9
[arXiv:1306.3436 [hep-ph]].
\citations{37 citations counted in INSPIRE as of 25 Mar 2020}

%\cite{Lonnblad:2012ng}
\item%{Lonnblad:2012ng}
L.~Lonnblad and S.~Prestel,
\emph{``Unitarising Matrix Element + Parton Shower merging''},
JHEP \textbf{02} (2013), 094
doi:10.1007/JHEP02(2013)094
[arXiv:1211.4827 [hep-ph]].
\citations{65 citations counted in INSPIRE as of 25 Mar 2020}  {\bf (Part of PhD thesis)}

%\cite{Lonnblad:2012ix}
\item%{Lonnblad:2012ix}
L.~L{\"o}nnblad and S.~Prestel,
\emph{``Merging Multi-leg NLO Matrix Elements with Parton Showers''},
JHEP \textbf{03} (2013), 166
doi:10.1007/JHEP03(2013)166
[arXiv:1211.7278 [hep-ph]].
\citations{161 citations counted in INSPIRE as of 25 Mar 2020}  {\bf (Part of PhD thesis)}



%\cite{Lonnblad:2011xx}
\item%{Lonnblad:2011xx}
L.~Lonnblad and S.~Prestel,
\emph{``Matching Tree-Level Matrix Elements with Interleaved Showers''},
JHEP \textbf{03} (2012), 019
doi:10.1007/JHEP03(2012)019
[arXiv:1109.4829 [hep-ph]].
\citations{152 citations counted in INSPIRE as of 25 Mar 2020}  {\bf (Part of PhD thesis)}

\end{enumerate}

\noindent
\large{\bf 2.2~~\,Peer-reviewed conference contributions}: None, for which the results are not included in other publications.

\setcounter{subsection}{2}

\subsection{Peer-reviewed edited volumes}

\begin{enumerate}
\item%{Amoroso:2020lgh}
S.~Amoroso {\it et al.},
\emph{``Les Houches 2019: Physics at TeV Colliders: Standard Model Working Group Report''},
[arXiv:2003.01700 [hep-ph]].
\citations{1 citations counted in INSPIRE as of 30 Mar 2020}
%\cite{Cepeda:2019klc}
\item%{Cepeda:2019klc}
M.~Cepeda {\it et al.},
\emph{``Higgs Physics at the HL-LHC and HE-LHC''},
CERN Yellow Rep.\ Monogr.\  \textbf{7} (2019), 221-584
doi:10.23731/CYRM-2019-007.221
[arXiv:1902.00134 [hep-ph]].
\citations{140 citations counted in INSPIRE as of 31 Mar 2020}
%\cite{Bendavid:2018nar}
\item%{Bendavid:2018nar}
J.~Andersen {\it et al.},
\emph{``Les Houches 2017: Physics at TeV Colliders Standard Model Working Group Report''},
[arXiv:1803.07977 [hep-ph]].
\citations{69 citations counted in INSPIRE as of 25 Mar 2020}
%\cite{Badger:2016bpw}
\item%{Badger:2016bpw}
J.~Andersen {\it et al.},
\emph{``Les Houches 2015: Physics at TeV Colliders Standard Model Working Group Report''},
[arXiv:1605.04692 [hep-ph]].
\citations{122 citations counted in INSPIRE as of 25 Mar 2020}
%\cite{Andersen:2014efa}
\item%{Andersen:2014efa}
J.~Andersen {\it et al.},
\emph{``Les Houches 2013: Physics at TeV Colliders: Standard Model Working Group Report''},
[arXiv:1405.1067 [hep-ph]].
\citations{168 citations counted in INSPIRE as of 25 Mar 2020}
%\cite{AlcarazMaestre:2012vp}
\item%{AlcarazMaestre:2012vp}
J.~Alcaraz Maestre \textit{et al.} [SM, NLO MULTILEG Working Group and SM MC Working Group],
\emph{``The SM and NLO Multileg and SM MC Working Groups: Summary Report''},
[arXiv:1203.6803 [hep-ph]].
\citations{123 citations counted in INSPIRE as of 25 Mar 2020}
\end{enumerate}

\subsection{Research review articles}

\begin{enumerate}
%\cite{Buckley:2019kjt}
\item%{Buckley:2019kjt}
A.~Buckley {\it et al.},
\emph{``Monte Carlo event generators for high energy particle physics event simulation''},
[arXiv:1902.01674 [hep-ph]].
\citations{3 citations counted in INSPIRE as of 25 Mar 2020}
\end{enumerate}

\noindent
%\large{\bf 2.5~~\,Peer-reviewed books and book chapters or other publications including popular science books/presentations}: None.
\large{\bf 2.5~~\,Peer-reviewed books and book chapters}: None.

\setcounter{subsection}{5}

\subsection{Other publications including popular science books/presentations}

\begin{enumerate}
%\cite{Buckley:2019kjt}
\item%{Buckley:2019kjt}
\emph{ ``DIRE: A C++ plugin for all-order corrections in PYTHIA''}
  \\{}S.~Prestel,~ \href{http://dire.gitlab.io}{dire.gitlab.io} and \href{https://gitlab.com/dire}{gitlab.com/dire}
%(Feb 5, 2019)
%\inspireurl{http://inspirehep.net/record/1718574}
\end{enumerate}









\end{document}
\grid
