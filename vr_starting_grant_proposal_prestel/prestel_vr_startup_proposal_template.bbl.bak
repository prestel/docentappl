\begin{thebibliography}{10}

\bibitem{Frixione:2002ik}
S.\ Frixione, B.~R. Webber.
\newblock {Matching NLO QCD computations and parton shower simulations}.
\newblock {\em JHEP}, 06:029, 2002.

\bibitem{Lonnblad:2001iq}
L.\ L{\"o}nnblad.
\newblock {Correcting the colour-dipole cascade model with fixed order matrix
  elements}.
\newblock {\em JHEP}, 05:046, 2002.

\bibitem{Sjostrand:2016bif}
{Sj{\"o}strand, T.}
\newblock {Status and developments of event generators}.
\newblock {\em PoS}, LHCP2016:007, 2016.

\bibitem{Li:2016yez}
H.~T. Li, P.\ Skands.
\newblock {A framework for second-order parton showers}.
\newblock {\em Phys. Lett.}, B771:59--66, 2017.

\bibitem{Hoche:2017iem}
S.~{H{\"o}che, S.~Prestel}.
\newblock {Triple\,collinear\,emissions\,in\,parton\,showers}.
\newblock {\em Phys. Rev.}, D96(7):074017, 2017.

\bibitem{Kallweit:2014xda}
S.\ Kallweit, J.~M. Lindert, P.\ Maierhofer, S.\ Pozzorini, and M.\ Schonherr.
\newblock {NLO electroweak automation and precise predictions for W+multijet
  production at the LHC}.
\newblock 2014.

\bibitem{Christiansen:2014kba}
{J.\,R.\,Christiansen, S.\,Sj{\"o}strand}.
\newblock {Weak Gauge Boson Radiation in Parton Showers}.
\newblock {\em JHEP}, 04:115, 2014.

\bibitem{Chen:2016wkt}
J.\ Chen, T.\ Han, B.\ Tweedie.
\newblock {Electroweak Splitting Functions and High Energy Showering}.
\newblock {\em JHEP}, 11:093, 2017.

\bibitem{Fuks:2016ftf}
B.\ Fuks, H.-S.\ Shao.
\newblock {QCD next-to-leading-order predictions matched to parton showers for
  vector-like quark models}.
\newblock {\em Eur. Phys. J.}, C77(2):135, 2017.

\bibitem{Hoche:2014kca}
S.~Schumann F.~Siegert S.~H{\"o}che, S.~Kuttimalai.
\newblock {Beyond Standard Model calculations with Sherpa}.
\newblock {\em Eur. Phys. J.}, C75(3):135, 2015.

\bibitem{Carloni:2010tw}
{L.~Carloni, S~Sj{\"o}strand}.
\newblock {Visible Effects of Invisible Hidden Valley Radiation}.
\newblock {\em JHEP}, 09:105, 2010.

\bibitem{Akesson:2018vlm}
T.~{\AA}kesson et~al.
\newblock {Light Dark Matter eXperiment (LDMX)}.

\bibitem{Marzani:2019hun}
M.~Spannowsky S.~Marzani, G.~Soyez.
\newblock {Looking inside jets: an introduction to jet substructure and
  boosted-object phenomenology}.
\newblock arXiv:1901.10342 [hep-ph]

\bibitem{Soper:2011cr}
D.~E. Soper, M.\ Spannowsky.
\newblock {Finding physics signals with shower deconstruction}.
\newblock {\em Phys. Rev.}, D84:074002, 2011.

\bibitem{Christiansen:2015jpa}
J.~R.\ Christiansen, S.\ Prestel.
\newblock {Merging weak and QCD showers with matrix elements}.
\newblock {\em Eur. Phys. J. C}, 76(1):39, 2016.

\bibitem{Prestel:2019neg}
S.~Prestel, M.~Spannowsky.
\newblock {HYTREES: Combining Matrix Elements and Parton Shower for Hypothesis
  Testing}.
\newblock arXiv:1901.11035 [hep-ph].

\bibitem{Kallweit:2015dum}
S.\ Kallweit, J.~M. Lindert, P.\ Maierhofer, S.\ Pozzorini, M.\ Schonherr.
\newblock {NLO QCD+EW predictions for V + jets including off-shell vector-boson
  decays and multijet merging}.
\newblock {\em JHEP}, 04:021, 2016.

\bibitem{Strassler:2006im}
M.~J. Strassler and K.~M. Zurek.
\newblock {Echoes of a hidden valley at hadron colliders}.
\newblock {\em Phys. Lett.}, B651:374--379, 2007.

\bibitem{ArkaniHamed:2008qn}
N.~Arkani-Hamed, D.~P. Finkbeiner, T.~R. Slatyer, N.~Weiner.
\newblock {A Theory of Dark Matter}.
\newblock {\em Phys. Rev.}, D79:015014, 2009.

\bibitem{Kondo:1988yd}
K.~Kondo.
\newblock {Dynamical Likelihood Method for Reconstruction of Events With
  Missing Momentum. 1: Method and Toy Models}.
\newblock {\em J. Phys. Soc. Jap.}, 57:4126--4140, 1988.

\bibitem{Datta:2017rhs}
K.~Datta, A.~Larkoski.
\newblock {How Much Information is in a Jet?}
\newblock {\em JHEP}, 06:073, 2017.

\end{thebibliography}
